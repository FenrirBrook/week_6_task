from datetime import datetime
class Summary:
    def __init__(self, title, id, attended_participants, start_time, end_time, hours, minutes, seconds):
        self.title = title
        self.id = id
        self.attended_participants = attended_participants
        self.start_time = start_time
        self.end_time = end_time
        self.duration = Duration(hours, minutes, seconds)

    def __str__(self):
        return {
            'Title': self.title,
            'Id': self.id,
            'Attended participants': self.attended_participants,
            'Start Time': self.start_time,
            'End Time': self.end_time,
            'Duration': self.duration.__str__()
        }
class Duration:
    def __init__(self, hours, minutes, seconds):
        self.hours = hours
        self.minutes = minutes
        self.seconds = seconds

    def __str__(self):
        return {
            'hours': self.hours,
            'minutes': self.minutes,
            'seconds': self.seconds,
        }

def get_duration(start, end):
    start_time = datetime.strptime(start, '%m/%d/%y, %I:%M:%S %p')
    end_time = datetime.strptime(end, '%m/%d/%y, %I:%M:%S %p')
    meeting_duration = end_time - start_time
    seconds = meeting_duration.seconds
    hours, rest = divmod(seconds, 3600)
    minutes, seconds = divmod(rest, 60)

    duration = {
        'hours':hours, 
        'minutes':minutes, 
        'seconds':seconds
        }
    return duration

def normalize_summary(raw_data: dict):
    
    if raw_data.get('Meeting Title'):
        duration = get_duration(raw_data.get('Meeting Start Time'),raw_data.get('Meeting End Time'))
        result = Summary(
            title = raw_data.get('Meeting Title'),
            id = raw_data.get('Meeting Id'),
            attended_participants = int(raw_data.get('Total Number of Participants')),
            start_time = raw_data.get('Meeting Start Time'),
            end_time = raw_data.get('Meeting End Time'),
            hours = duration.get('hours'),
            minutes = duration.get('minutes'),
            seconds = duration.get('seconds')
        )
        return result.__str__()
        
    duration = get_duration(raw_data.get('Start time'),raw_data.get('End time'))
    result = Summary(
        title= raw_data.get('Meeting title'),
        id = raw_data.get('Meeting title'),
        attended_participants=int(raw_data.get('Attended participants')),
        start_time=raw_data.get('Start time'),
        end_time=raw_data.get('End time'),
        hours=duration.get('hours'),
        minutes=duration.get('minutes'),
        seconds=duration.get('seconds')
    )
    return result.__str__()

def build_summary_object(raw: dict):

    result = Summary(
        title= raw.get('Title'),
        id = raw.get('Id'),
        attended_participants=int(raw.get('Attended participants')),
        start_time=raw.get('Start Time'),
        end_time=raw.get('End Time'),
        hours = raw['Duration']['hours'],
        minutes = raw['Duration']['minutes'],
        seconds = raw['Duration']['seconds']
    )
    return result
